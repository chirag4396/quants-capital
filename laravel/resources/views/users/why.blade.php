@extends('users.layouts.master')

@section('content')
<main role="main" class="probootstrap-main js-probootstrap-main bg4">
	<div class="probootstrap-bar">
		<a href="#" class="probootstrap-toggle js-probootstrap-toggle"><span class="oi oi-menu"></span></a>
		<div class="probootstrap-main-site-logo"><a href="index.html"><img class="res-logo" src="images/logo.png"></a></div>
	</div>
	<div class="quants-main ">
		@include('users.includes.side-buttons')
		<section class="insights">
			<div class="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">

							<div class="content">
								<h1> Why<span> Quants Capital</span></h1>
								<p>At Quants Capital, we understand that your investment portfolio  means a lot  more to you than just Rupees.It's about your goals for the future,  and the ability to live your dreams.
								That's why we manage each portfolio Using extensive Research and Market analysis,backed by sound investment principle.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<h3 class="title-new">BENEFITS TO YOU</h3>
		<section class="probootstrap-section">
			<div class="container-fluid">
				<div class="row mb-5 mb20">

					<div class="col-lg-4 col-md-6 b-rr">
						<div class="media d-block mb-4 text-left probootstrap-media p-md-5 p-4">

							<div class="media-body">
								<div class="a1">A Human Touch        </div>
								<p>We believe that our clients are the reason for our existence. That’s why we provide our best service possible.</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 b-rr">
						<div class="media d-block mb-4 text-left probootstrap-media p-md-5 p-4">

							<div class="media-body">
								<div class="a1">Innovative Technology        </div> 
								<p>Our state of the art technology provides you detailed analysis of the market with the help of our unique strategies.</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="media d-block mb-4 text-left probootstrap-media p-md-5 p-4">

							<div class="media-body">
								<div class="a1">Expert Advice        </div>
								<p>With our vast experience in financial field we provide you advice that guarantees no conflict of interest.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</main>
@endsection