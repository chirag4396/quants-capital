@extends('users.layouts.master')

@section('content')
<main role="main" class="probootstrap-main js-probootstrap-main">
	<div class="probootstrap-bar">
		<a href="#" class="probootstrap-toggle js-probootstrap-toggle">
			<span class="oi oi-menu">
			</span>
		</a>		
		<div class="probootstrap-main-site-logo">
			<a href="index.html">
				<img class="res-logo" src="images/logo.png">
			</a>
		</div>
	</div>

	<div class="quants-main ">		
		@include('users.includes.side-buttons')		
		<section class="insights">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="content">
							<h1>We are excited to
								<span> Hear You.</span>
							</h1>
							<p>Discover a firm that knows it’s all about you. Simply put, you are our reason for being.</p>
							<p class="dec1" >Contact us to experience the difference.</p>
						</div>
						<div class="row justify-content-center con">  
							<div class="col-xl-10 col-lg-12 mx-auto">  
								<div class="row">
									<div class="container-fluid">
										<div class="row mb-5">
											<div class="col-lg-12 col-md-12">
												<div class="media d-block mb-4 text-left probootstrap-media  p-4">
													<div class="media-body">
														<div class="c-left ">
															<img class="img3" src="images/q1.png">
														</div>
														<div class="c-right ">
															<h5 class="mt-0">Kumar Business Centre, Level - 6, Bund Garden Rd, Behind ICICI Bank, Pune, Maharashtra 411001
															</h5>
														</div>
														<div class="clearfix"></div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6">
												<div class="media d-block mb-4 text-left probootstrap-media  p-4">
													<div class="media-body ">
														<div class="c-left ">
															<img class="img4" src="images/q2.png">
														</div>
														<div class="c-right ">
															<h5 class="mt-0">(020) - 66894040/41/42.</h5>
														</div>
														<div class="clearfix"></div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6">
												<div class="media d-block mb-4 text-left probootstrap-media  p-4">
													<div class="media-body">
														<div class="c-left ">
															<img class="img4" src="images/q3.png">
														</div>
														<div class="c-right ">
															<h5 class="mt-0">info@quantscapital.com</h5>
														</div>
														<div class="clearfix"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</main>

@endsection