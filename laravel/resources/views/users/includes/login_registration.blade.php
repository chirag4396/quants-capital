<div class="modal fade" id="regModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">New User registration</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<section id="contact">
					<div class="contact-section">
						<div class="">
							<form id = "regForm">
								<div class="col-md-12 form-line">
									<div class="col-md-12">
										<div class="form-group">
											<input type="text" class="form-control1" placeholder="Enter Name" name="name" data-validate = "empty|alphaSpace">
										</div>
										<div class="form-group">
											<input type="text" class="form-control1" placeholder="Email ID" name="email" data-validate = "empty|email">
										</div> 
										<div class="form-group">
											<input type="number" min = "7000000000" max = "9999999999" name="ud[mobile]" class="form-control1" placeholder="Mobile Number"  data-validate = "empty|mobile">
										</div> 
										{{-- <div class="form-group">
											<label>Date of Birth</label>
											<input type="date" name="ud[dob]" class="form-control1">
										</div>  --}}
										<div class="form-group">
											<input id="password" type="password" class="form-control" name="password" required placeholder="Password" data-validate = "empty|password">
										</div>
										<div class="form-group">											
											<select class="form-control" id = "securitySelect" name="ud[security_question]" >
												<option value = "-1">Security Question</option>
												@forelse (\App\SecurityQuestion::get() as $sq)
												<option value="{{ $sq->sq_id }}">{{ $sq->sq_qus }}</option>
												@empty										    		
												@endforelse
											</select>
										</div>
										<div class="form-group hidden" id = "securityAnswer">
											<input type="text" class="form-control" name="ud[security_answer]" required placeholder="Security Answer" data-validate = "empty">
										</div>										
										<div class="clearfix"></div>
										<div class="col-md-12 text-center" style="padding:0px;">
											<button type = "submit" class="btn btn-primary "><i class="fa fa-paper-plane" aria-hidden="true"></i>  Submit</button>
										</div>
										<div class="clearfix"></div>
									</div>									
								</div>
							</form>
						</div>
					</div>
				</section>
			</div>			
		</div>
	</div>
</div>

<div class="modal fade" id="loginModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Log In</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<section id="contact">
					<div class="contact-section">
						<div class="">
							<form id = "loginForm">
								<div class="col-md-12 form-line">
									<div class="col-md-12">
										<div class="form-group">
											<input type="text" class="form-control1" name="email" placeholder="Email">
										</div>
										<div class="form-group">
											<input type="password" class="form-control1" name="password" placeholder="Password">
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12" style="padding:0px;">
											<div class="qa">
												<a class="btn btn-link" href="{{ route('password.request') }}">
													{{ __('Forgot Your Password?') }}
												</a>
											</div>
											<button type = "submit" class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>  Login</button>
										</div>
										<div class="clearfix"></div>										
									</div>									
								</div>
							</form>
						</div>
					</div>
				</section>
			</div>			
		</div>
	</div>
</div>

@push('footer')
<script type="text/javascript" src = "{{ asset('js/crud.js') }}"></script>
<script type="text/javascript">
	$('#securitySelect').on({
		'change' : function(){
			if($(this).val() != "-1"){
				$('#securityAnswer').removeClass('hidden');
			}else{
				$('#securityAnswer').addClass('hidden');					
			}
		}
	});
	$('#regForm').CRUD({
		url : '{{ route('register') }}',
		processResponse : function (data) {
			if(data.msg == "successRegister"){
				window.setTimeout(function(){
					location.reload();
				},2500);
			}
		},
		onLoad: 'check'
	});

	$('#loginForm').CRUD({
		url : '{{ route('login') }}',
		processResponse : function (data) {			
			if(data.msg == "successLogin"){
				window.setTimeout(function(){
					location.href = '{{ route("admin.home") }}';
				},1500);
			}
		},
		onLoad: 'check'
	});
</script>
@endpush