<div class="forms">
	@guest
	<button type="button" data-toggle="modal" data-target="#loginModal" class="btn book login-btn" >Log In</button>
	<button type="button" data-toggle="modal" data-target="#regModal" class="btn book login-btn">Register Here</button>	
	@else
	<a class="btn book login-btn" href="{{ route('admin.home') }}">Dashboard</a>
	<a class="btn book login-btn" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
	    {{ __('Logout') }}
	</a>
	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	    @csrf
	</form>
	@endguest
</div>