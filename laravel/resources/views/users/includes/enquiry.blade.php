<div class="modal fade" id="enqModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<section id="contact">
					<div class="contact-section">
						<div class="">
							<form id = "enqForm">
								<div class="col-md-12 form-line">
									<div class="col-md-12">
										<input type="hidden" name="ie_id" id = "enq_id">
										<div class="form-group">
											<input type="text" class="form-control1" name="name" placeholder="Full Name">
										</div>
										<div class="form-group">
											<input type="text" class="form-control1" name="email" placeholder="Email">
										</div>
										<div class="form-group">
											<input type="number" min = "7000000000" max = "9999999999" class="form-control1" name="mobile" placeholder="Mobile Number">
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12 text-center" style="padding:0px;">
											<button class="btn btn-primary"><i class="fa fa-paper-plane" aria-hidden="true"></i>Enquiry</button>
										</div>
										<div class="clearfix"></div>										
									</div>									
								</div>
							</form>
						</div>
					</div>
				</section>
			</div>			
		</div>
	</div>
</div>

@push('footer')
<script type="text/javascript" src = "{{ asset('js/crud.js') }}"></script>
<script type="text/javascript">
	function openEnquiry(id, name){
		$('#enqModal').modal('toggle');
		$('#enqModal h5').html('Enquiry for '+name);
		$('#enq_id').val(id);
	}
	$('#enqForm').CRUD({
		url:'{{ route('enquiry.store') }}',
		processResponse : function(data){
			console.log(data);
		}
	});
</script>
@endpush