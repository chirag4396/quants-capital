<!DOCTYPE html>
<html lang="en">
<head>
 <title>Quants Capital</title>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 <meta name="description" content="Quants Capital" />
 <meta name="keywords" content="Quants Capital" />
 <meta name="csrf-token" content="{{ csrf_token() }}">
 <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
 <link rel="stylesheet" href="{{ asset('css/open-iconic-bootstrap.min.css') }}">

 <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
 <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
 <link rel="stylesheet" href="{{ asset('css/icomoon.css') }}">
 <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
 <link rel="stylesheet" href="{{ asset('css/style.css') }}">
 <link rel="stylesheet" href="{{ asset('css/quant.css') }}">

 @stack('header')
</head>
<body>
  @if (Session::has('approval'))    
  <div class="approval">You need Approval to view reports <span>x</span></div>
  @endif
  <aside class="probootstrap-aside js-probootstrap-aside">
    <a href="#" class="probootstrap-close-menu js-probootstrap-close-menu d-md-none"><span class="oi oi-arrow-left"></span> Close</a>
    <div class="probootstrap-site-logo probootstrap-animate fadeInLeft probootstrap-animated" data-animate-effect="fadeInLeft">
      <a href="{{ route('home') }}" class="logo">V3</a>
    </div>
    <div class="probootstrap-overflow">
      <nav class="probootstrap-nav">
        <ul>
          <li><a href="{{ route('why') }}">Why Quants Capital</a></li>
          @php
            $currentRoute = Route::currentRouteName();
          @endphp
          @if ($currentRoute == 'what-we-offer')
          <li class="menu1">  
            <button class="dropdown-btn active"  href="{{ route('what-we-offer') }}">What we Offer 
              <span style="text-align: right;" class="icon-triangle-down"></span>
            </button>
            <div class="dropdown-container" style="display: block;">
              <a href="#QUANTS">QUANTS ALPHA</a>
              <a href="#PORTFOLIO">PORTFOLIO MANAGEMENT</a>
              <a href="#ALTERNATIVE">ALTERNATIVE INVESTMENTS</a>
              <a href="#TREASURY">TREASURY MANAGEMENT</a>
              <a href="#RISK">RISK MANAGEMENT</a>
            </div>
          </li>
          @else
          <li class="menu1"><a href="{{ route('what-we-offer') }}">What we Offer</a></li>
          @endif
          <li><a href="{{ route('about') }}">About US</a></li>
          <li><a href="{{ route('insight') }}">Insights</a></li>
          <li><a href="{{ route('contact') }}">Contact Us</a></li>
        </ul>
      </nav>
      <footer class="probootstrap-aside-footer">
        <ul class="list-unstyled d-flex probootstrap-aside-social text-center">
          <li><a href="#" class="p-2"><span class="icon-linkedin"></span></a></li>
          <li><a href="#" class="p-2"><span class="icon-twitter"></span></a></li>
        </ul>
        <p>&copy; 2018 <a href="http://sungare.com/" target="_blank">Sungare Technologies</a>. <br> All Rights Reserved.</p>
      </footer>
    </div>
  </aside>