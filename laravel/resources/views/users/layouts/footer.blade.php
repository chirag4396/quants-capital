@guest
@include('users.includes.login_registration')
@endguest
<script src="{{ asset('js/jquery.3.3.1.min.js') }}"></script>

<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>

<script type="text/javascript">
  $(window).scroll(function() { 
    var scroll = $(".quants-target").scrollTop();

    if (scroll >= 0) {
      $( ".quants-target__inner" ).each(function() {
        $(this).css({
          transform: "translate3d(-50%, -50%, 0)"            
        })          
      });

      $( ".quants-target__copy" ).each(function() {
        $(this).css({
          opacity: "1"            
        })          
      });        
    }      
  });
  $('.approval span').on({
    'click' : function(){
      $('.approval').toggle();
    }
  });
</script>
@stack('footer')

</body>
</html>