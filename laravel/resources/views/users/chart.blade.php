
<script src="{{ asset('admin-assets/jquery/dist/jquery.min.js') }}"></script>

<script>
	var dataPoints = []
	window.onload = function () {

		var chart = new CanvasJS.Chart("chartContainer", {
			animationEnabled: true,
			title:{
				text: "Daily High Temperature at Different Beaches"
			},
			axisX: {
				valueFormatString: "DD MMM,YY"
			},
			legend:{
				cursor: "pointer",
				fontSize: 16,
				itemclick: toggleDataSeries
			},
			toolTip:{
				shared: true
			},
			data: dataPoints
	// [{
	// 	name: "Myrtle Beach",
	// 	type: "spline",
	// 	yValueFormatString: "#0.## °C",
	// 	showInLegend: true,
	// 	dataPoints: [
	// 		{ x: new Date(2017,6,24), y: 31 },
	// 		{ x: new Date(2017,6,25), y: 31 },
	// 		{ x: new Date(2017,6,26), y: 29 },
	// 		{ x: new Date(2017,6,27), y: 29 },
	// 		{ x: new Date(2017,6,28), y: 31 },
	// 		{ x: new Date(2017,6,29), y: 30 },
	// 		{ x: new Date(2017,6,30), y: 29 }
	// 	]
	// },
	// {
	// 	name: "Martha Vineyard",
	// 	type: "spline",
	// 	yValueFormatString: "#0.## °C",
	// 	showInLegend: true,
	// 	dataPoints: [
	// 		{ x: new Date(2017,6,24), y: 20 },
	// 		{ x: new Date(2017,6,25), y: 20 },
	// 		{ x: new Date(2017,6,26), y: 25 },
	// 		{ x: new Date(2017,6,27), y: 25 },
	// 		{ x: new Date(2017,6,28), y: 25 },
	// 		{ x: new Date(2017,6,29), y: 25 },
	// 		{ x: new Date(2017,6,30), y: 25 }
	// 	]
	// },
	// {
	// 	name: "Nantucket",
	// 	type: "spline",
	// 	yValueFormatString: "#0.## °C",
	// 	showInLegend: true,
	// 	dataPoints: [
	// 		{ x: new Date(2017,6,24), y: 22 },
	// 		{ x: new Date(2017,6,25), y: 19 },
	// 		{ x: new Date(2017,6,26), y: 23 },
	// 		{ x: new Date(2017,6,27), y: 24 },
	// 		{ x: new Date(2017,6,28), y: 24 },
	// 		{ x: new Date(2017,6,29), y: 23 },
	// 		{ x: new Date(2017,6,30), y: 23 }
	// 	]
	// }]
});
		chart.render();
		$.getJSON("{{ route('monthly-performance') }}", function(data) {  
  // var da = [];
  var fetch = ['mp_dns', 'mp_nifty', 'mp_nifty_dns', 'mp_fd', 'mp_fd_dns'];

  $.each(fetch, function(fKey, fValue){

  	var res = [];

  	$.each(data, function(key, value){
  		var date = value.mp_date.split('-');
  		res.push(
  			{
			x: new Date(date[0],date[1], date[2]),
  			y: parseInt(value[fValue])
  		}
  		);
  	});

  	dataPoints.push(
  		{
  			name: "Nantucket",
  			type: "spline",
  			yValueFormatString: "##.## %",
  			showInLegend: true,
  			dataPoints:res
  		}
  	// {
   //        // name : fValue.split('_')[1],
   //        // type : 'spline',
   //        visible:true,
   //        yValueFormatString : '#0.## %',
   //        showInLegend : true,
   //        dataPoints : res
   //    }
      );    
    // $.each(data, function(key, value){
    //   res.push({
    //     x: new Date(value.mp_date),
    //     y: value[fValue]
    //   });
    // });
    
    // dataPoints.push({
    //   // name : fValue.split('_')[1],
    //   // type : 'spline',
    //   visible:true,
    //   yValueFormatString : '#0.## %',
    //   showInLegend : true,
    //   dataPoints : res
    // });
    // da.push();
});
  // console.log(dataPoints);
  // datapoint = da;
  chart.render();
});
		function toggleDataSeries(e){
			if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
				e.dataSeries.visible = false;
			}
			else{
				e.dataSeries.visible = true;
			}
			chart.render();
		}

	}
</script>
</head>
<body>
	<div id="chartContainer" style="height: 300px; width: 100%;"></div>
	<script src="{{ asset('js/canvasjs.min.js') }}"></script>
</body>
</html>