@extends('users.layouts.master')

@push('header')
@endpush

@section('content')
<main role="main" class="probootstrap-main js-probootstrap-main bg5">
 <div class="probootstrap-bar">
  <a href="#" class="probootstrap-toggle js-probootstrap-toggle"><span class="oi oi-menu"></span></a>
  <div class="probootstrap-main-site-logo"><a href="index.html"><img class="res-logo" src="images/logo.png"></a></div>
</div>
<div class="quants-main ">

  @include('users.includes.side-buttons')

  <section class="insights">
    <div class="">
     <div class="container what">
      <div class="row">
        <div class="col-md-12">

          <div class="content">
            <h1> What <span>We Offer</span></h1>
            <p>With our products and services, we try to fulfil your goals and objectives, with a dedication to protect and grow our investors’ wealth.
            To achieve this, we provide right product mix to cater  the needs of investors from various categories.</p>
          </p>
        </div>

        <div class="col-md-12 " id="QUANTS">
         <h3 class="title-new1 ">
          QUANTS ALPHA

        </h3>
        <div class="col-md-5"><img src="images/qqq.png"></div>
        <div class="col-md-7"><p>Our flagship strategy is Quants Alpha which lets our investors generate additional returns on their existing portfolio irrespective of the market direction.
          It is a market neutral Long-Short strategy on Index based structures.
        We design, analyse  and execute short-term quantitative strategies using our proprietary algorithms and softwares.</p>
      </div>            

    </div>

    <div class="col-md-12 text-center ">
      <button type="button" class="btn1">  View 
      Historic Performance </button>
    </div>

    <div class="his" style="display: none;">

      <div class="row"><div class="col-sm-12"><table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
        <thead>
          <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 152px;"></th><th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 238px;">Quants Alpha</th><th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 110px;">Index</th><th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 190px;">Index + Quants Alpha </th><th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 106px;">FD</th><th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 170px;">FD + Quants Alpha</th></tr>
        </thead>
        <tfoot>
          <tr>

            <th rowspan="1" colspan="2" style="text-align: center;">Out perfomance</th>
            <th rowspan="1" colspan="4" style="text-align: center;">13.73%</th>


          </tr>
        </tfoot>
        <tbody>
          @forelse ($monthlyPerformances as  $mp)
          <tr>
            <td>{{ \Carbon\Carbon::parse($mp->mp_date)->format('M-y') }}</td>
            <td>{{ $mp->mp_dns }}%</td>
            <td>{{ $mp->mp_nifty }}%</td>
            <td>{{ $mp->mp_nifty_dns }}%</td>
            <td>{{ $mp->mp_fd }}%</td>
            <td>{{ $mp->mp_fd_dns }}%</td>
          </tr>
          @empty          
          @endforelse         
        </tbody>
      </table>
    </div>
  </div>

  {{-- <div class="card img-loaded"> --}}
    <!-- <img class="card-img-top probootstrap-animate fadeIn probootstrap-animated" src="images/va.png" alt="Card image cap" data-animate-effect="fadeIn"> -->

    <div id="chartContainer" style="height: 400px;width: 100%;"></div>

  {{-- </div> --}}

</div>

<div class="clearfix"></div>
<div id="PORTFOLIO" class="col-md-12 mbt"><img src="images/22.png"></div>
<div class="col-md-12 port ">

  <h3 class="title-new1 ">PORTFOLIO MANAGEMENT</h3>

  <img src="images/port.png">
  <div class="clearfix"></div>

  <div class="col-md-12  mb  "> 
    <span>GOAL ANALYSIS</span>
    <p>Understanding the clients’ requirements and future needs, our investment strategies are decided.</p>
  </div>


  <div class="col-md-12  mb ">  
    <span>RISK ASSESSMENT</span>
    <p>We focus much of our attention on the Client’s Risk Profile, which includes a clear understanding of:</p>
    <ul>
      <li>Risk Capacity – the degree of risk that can be taken from a pure financial perspective.</li>
      <li>Risk Tolerance – the amount of risk a client “willing” to take.</li> 
      <li> Risk Demand – The level of risk that “needs” to be taken in order to accomplish the goals set by the client.</li>
    </ul>
  </div>

  <div class="col-md-12   mb "> 
    <span>ASSET ALLOCATION</span>

    <p>The asset allocation decision is the most important determinant of variation in returns. Therefore, we concentrate efforts on the total portfolio composition and how assets are allocated.</p>
  </div>


  <div class="col-md-12   mb "> 
    <span>MANAGING RISK</span>

    <p>We perform identification, analysis and acceptance/mitigation of uncertainty in investment decisions. The risk is measured and the portfolio is diversified. Decisions to add new asset classes are based on how they impact the overall portfolio’s expected return, volatility and downside risk.</p>
  </div>

  <div class="col-md-12  mb  "> 
    <span>IMPACT OF TAXES</span>

    <p>We construct portfolios in a tax efficient manner. </p>
  </div>


  <div class="col-md-12   "> 
    <span>INDIVIDUALLY DESIGNED PORTFOLIOS</span>

    <p>

    We construct a portfolio unique to your individual needs, rather than mould your needs to a pre-formed package. Your tolerance of risk, as well as your need for return, is conscientiously considered in our design. Ongoing management takes into account the client’s specific cash flows and taxation issues.</p>
  </div>

  <div class="clearfix"></div>
</div>



<div class="clearfix"></div>
<div class="col-md-12 mbt"><img src="images/22.png"></div>
<div class="col-md-12  " id="ALTERNATIVE"> 

  <h3 class="title-new1 ">ALTERNATIVE INVESTMENTS</h3>
  <div class="col-md-3"><img src="images/inv.png"></div>
  <div class="col-md-9">
    <p>Apart from the conventional investment instruments, we provide some new investment avenues such as private equity, hedge funds, managed futures and derivatives contracts with the help of advanced software.</p>
  </div>

  <div class="clearfix"></div>
</div>

<div class="col-md-12 mbt"><img src="images/22.png"></div>
<div class="col-md-12  "   id="TREASURY"> 



  <h3 class="title-new1 " >TREASURY MANAGEMENT</h3>
  <div class="col-md-10">
    <p>We provide complete treasury management solution to the firms in which, we optimize their company's liquidity, make sound financial investments for the future with any excess cash, and reduce or enter into hedges against its financial risks using advanced technologies.</p>
  </div>
  <div class="col-md-2"><img src="images/tre.png"></div>
  <div class="clearfix"></div>
</div>


<div class="col-md-12 mbt"><img src="images/22.png"></div>
<div class="col-md-12"  id="RISK"> 

  <h3 class="title-new1 ">RISK MANAGEMENT</h3>
  <div class="col-md-2"><img src="images/ri.png"></div>
  <div class="col-md-10">
    <p>Financial plan is not likely to be done on disability, a premature death, a lawsuit, or a significant loss of property, but we all know these unfortunate circumstances do occur. Our role is to help you understand what things can disrupt your financial plans and develop strategies to minimize those risks.</p>
  </div>
  <div class="clearfix"></div>

</div>
</div>
</div>
</div>
</div>
</section>

</div>
</main>
@endsection


@push('footer')

{{-- <script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script> --}}
<script>

  var dropdown = document.getElementsByClassName("dropdown-btn");
  var i;

  for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var dropdownContent = this.nextElementSibling;
      if (dropdownContent.style.display === "block") {
        dropdownContent.style.display = "none";
      } else {
        dropdownContent.style.display = "block";
      }
    });
  }
  $(document).ready(function(){
    $('.btn1').click(function(){
      $('.his').toggle('slow');
    });
  });
</script>
<script src="{{ asset('js/canvasjs.min.js') }}"></script>

<script type="text/javascript">
  var dataPoints = [];
  window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer", {
      animationEnabled: true,
      width: 1065,
      axisX: {
        valueFormatString: "DD MMM,YY",
      },
      axisY: {
        title: "Value (in %)",
        // includeZero: false,
        // valueFormatString:"#",
        suffix: " %"
      },
      legend:{
        cursor: "pointer",
        fontSize: 16,
        itemclick: toggleDataSeries
      },
      toolTip:{
        shared: true
      },
      data : dataPoints
    });

    $.get("{{ route('monthly-performance') }}", function(data) {  
      var fetch = ['mp_dns', 'mp_nifty', 'mp_nifty_dns', 'mp_fd', 'mp_fd_dns'];
      var names = ['Quants Alpha', 'INDEX', 'INDEX + Quants Alpha', 'FD', 'FD + Quants Alpha'];

      $.each(fetch, function(fKey, fValue){

        var res = [];
        
        $.each(data, function(key, value){
          var date = value.mp_date.split('-');
          res.push(
          {
            x: new Date(date[0],parseInt(date[1])-1, date[2]),
            y: parseFloat(value[fValue])
          }
          );          
        });
        
        dataPoints.push({
          name : names[fKey],
          type : 'spline',
          visible:true,
          yValueFormatString : '0.##',
          showInLegend : true,
          dataPoints : res
        });
      });      
      chart.render();
    },'json');
    chart.render();


    function toggleDataSeries(e){
      if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
      }
      else{
        e.dataSeries.visible = true;
      }

      chart.render();
    }

  }
</script>


@endpush