@extends('users.layouts.master')

@section('content')

<div id="plans">
	<div id="particles-js"></div>

</div>

<main role="main" class="probootstrap-main js-probootstrap-main bg">
	<div class="probootstrap-bar">
		<a href="#" class="probootstrap-toggle js-probootstrap-toggle"><span class="oi oi-menu"></span></a>
		<div class="probootstrap-main-site-logo"><a href="index.html"><img class="res-logo" src="images/logo.png"></a></div>
	</div>
	<div class="quants-main ">
		@include('users.includes.side-buttons')
		<div class="quants-main insights ">
			<div class="container">
				<div class="row">
					<div class="col-md-12">

						<div class="content">
							<h1 class="main-h1"> Build , Grow And 
								<br> 
								<span> Nurture Wealth
								</span>
							</h1>
							<p class="p-main"> Quants Capital is dedicated to organized approach that helps you  to preserve and grow your wealth.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>

		<section class="quants-scene quants-scene--intro animate in-view insights "  data-element="quants-scene-exit">
			<div class="content">
				<h1> Single Point of Contact to manage your wealth.</h1>
			</div>
			<div class="quants-scene__stage"  data-element="scene-entered">
				<div class="quants-target">
					<div class="quants-target__inner"></div>
					<div class="quants-target__inner"></div>
					<div class="quants-target__inner"></div>
					<div class="quants-target__inner"></div>
					<div class="quants-target__inner"></div>

					<span class="quants-target__copy ">Alternative <br>Investment</span>
					<span class="quants-target__copy">Treasury <br>management</span>
					<span class="quants-target__copy">Dervatives  </span>
					<span class="quants-target__copy">commodities<br> trading</span>
					<span class="quants-target__copy">currency</span>
					<span class="quants-target__copy">Equity </span>
					<span class="quants-target__copy">Investment<br> Advisory</span>
					<span class="quants-target__copy">Portfolio <br> management </span>
					<span class="quants-target__copy">Algotrades</span>
					<span class="quants-target__copy">Low latency </span>
					<span class="quants-target__copy">Dynamic delta hedging</span>
					<span class="quants-target__copy">Risk Management</span>
					<span class="quant" style=""> Quants <br>Alpha</span>
					<span class="quants-target__copy"> Greek Analysis</span>
				</div>	        
			</div>	          
		</section>
	</div>
	<div class=" probootstrap-animate" data-animate-effect="fadeInDown" class="clearfix"></div>
</main>
@endsection

@push('footer')
	<script src="{{ asset('js/particles.js') }}"></script>
	<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
	<script src="{{ asset('js/main.js') }}"></script>
	<script src="{{ asset('js/app.js') }}"></script>
@endpush
