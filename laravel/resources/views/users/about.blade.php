@extends('users.layouts.master')

@section('content')
<main role="main" class="probootstrap-main js-probootstrap-main bg3">
	<div class="probootstrap-bar">
		<a href="#" class="probootstrap-toggle js-probootstrap-toggle"><span class="oi oi-menu"></span></a>
		<div class="probootstrap-main-site-logo"><a href="index.html"><img class="res-logo" src="images/logo.png"></a></div>
	</div>
	<div class="quants-main ">
		@include('users.includes.side-buttons')
		<section class="insights">
			<div class="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">

							<div class="content ">
								<h1>About <span> Quants Capital</span></h1>
								<p>Quants Capital was incorporated with a vision, to provide complete Wealth Management Solutions with real-time risk analysis using quantitative methods and advanced simulation software.
									We cater clients’ needs of providing the right alternative products mix   for our investors, with the focus on 100% algorithmic trading strategies. 
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="content">
							<img src="images/mm.png">
							<p >Every member of our team has valuable experience in respective field, with which we are developing our skills to serve you better.
							</p>
						</div>   
						<div class="card-columns">
							<div class="card img-loaded">
								<a href="single.html">
									<img class="card-img-top probootstrap-animate fadeIn probootstrap-animated" src="images/p1.jpg" alt="Card image cap" data-animate-effect="fadeIn">
								</a>
								<h3>Madhur Dahale</h3>

								<h4>Co-Founder and CEO</h4>
								Madhur, Founder and CEO of the firm is responsible for leading the development and execution of Firm’s long term strategies. He brings over 15 years of management consulting, operations and marketing expertise. Prior to starting Quants he was with Religare as Vice President, where he played a vital role in business development, capital raising, customer acquisition, event strategies, management consulting, marketing, product development, sales and technology integration.
								Madhur is Management student from Pune university and expertise in financial products
							</div>
							<div class="card img-loaded">
								<a href="single.html">
									<img class="card-img-top probootstrap-animate fadeIn probootstrap-animated" src="images/p2.jpg" alt="Card image cap" data-animate-effect="fadeIn">
								</a>
								<h3>Dhananjay  Gokhale</h3>

								<h4>Head Client Support and Acquisition</h4>


								Dhananjay brings in over 27 years of vast experience in Manufacturing, Finance, Dhananjay plays a vital role in company’s horizontal expansion in different territories.
								Dhananjay at his previous endeavours have worked with large MNC and has played an important role in their strategy planning as well as business development and client relations.
								Dhanajay a mechanical engineer has changed they way we work on Client relations
							</div>
							<div class="card img-loaded">
								<a href="single.html">
									<img class="card-img-top probootstrap-animate fadeIn probootstrap-animated" src="images/p3.jpg" alt="Card image cap" data-animate-effect="fadeIn">
								</a>
								<h3> Tanmay Kurtkoti</h3>

								<h4>Co-Founder and Chief Strategist</h4>


								Tanmay, Founder and Chief Strategist of the Firm specializes in the development of automated quantitative trading strategies. Prior to starting Quants he head various prop desks for over 4 years where he played a vital role in the development and implementation of various complex structures and high/medium frequency trading models, which contributed significantly to the prop des  profitability.
								He brings in over 10 years of experience from his previous stints at various broking firms where he worked closely in algorithmic trading, statistical arbitrage, quantitative modelling, back testing, strategy development analysis and risk handling.
								• Tanmay is a post graduate in Finance. Certified Market Professional from NSE and a CFA level 3 Candidate .

							</div>
							<div class="card img-loaded">
								<a href="single.html">
									<img class="card-img-top probootstrap-animate fadeIn probootstrap-animated" src="images/p4.jpg" alt="Card image cap" data-animate-effect="fadeIn">
								</a>
								<h3> Pratik Patil</h3>
								<h4>Business Development</h4>
								Pratik is enthusiastic Marketing Specialist to help us in our overall marketing efforts. He is an integral part of the development and execution of marketing plans to reach targets from brand awareness to product promotion. 
								Pratik is well-versed in specialized marketing concepts, principles and tactics. Earlier he was associate with Samco securities and Geojit BNP Paribas.
							</div>
							<div class="card img-loaded">
								<a href="single.html">
									<img class="card-img-top probootstrap-animate fadeIn probootstrap-animated" src="images/p5.jpg" alt="Card image cap" data-animate-effect="fadeIn">
								</a>
								<h3> Abhijeet Gosavi</h3>
								<h4>Head of operations</h4>
								Abhijeet is responsible for providing trading inputs for strategies and support in trade execution. He is having 10 years experience in capital market, equity, derivatives, portfolio management and risk management domain. His expertise in analysing technical parameters along with global and domestic economic data. Also, having vigorous network of well known fundamental analysts and super HNI’s.       
								Abhijeet has more than 5 years experience as a research analyst with Dalal Street Investment Journal.
							</div>
							<div class="card img-loaded">
								<a href="single.html">
									<img class="card-img-top probootstrap-animate fadeIn probootstrap-animated" src="images/p5.jpg" alt="Card image cap" data-animate-effect="fadeIn">
								</a>
								<h3> Abhijeet Gosavi</h3>
								<h4>Head of operations</h4>
								Abhijeet is responsible for providing trading inputs for strategies and support in trade execution. He is having 10 years experience in capital market, equity, derivatives, portfolio management and risk management domain. His expertise in analysing technical parameters along with global and domestic economic data. Also, having vigorous network of well known fundamental analysts and super HNI’s.       
								Abhijeet has more than 5 years experience as a research analyst with Dalal Street Investment Journal.
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</main>


@endsection