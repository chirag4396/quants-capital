@extends('users.layouts.master')

@section('content')
<main role="main" class="probootstrap-main js-probootstrap-main bg4">
	<div class="probootstrap-bar">
		<a href="#" class="probootstrap-toggle js-probootstrap-toggle"><span class="oi oi-menu"></span></a>
		<div class="probootstrap-main-site-logo"><a href="index.html"><img class="res-logo" src="images/logo.png"></a></div>
	</div>
	<div class="quants-main ">
		@include('users.includes.side-buttons')
		
		<section class="insights">
			<div class="">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="content">
								<h1> Insights</h1>
								<p>Filter our collection of news stories and curated web articles by category or simply browse through our latest thinking.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="tabbable-panel">
							<div class="tabbable-line">
								<ul class="nav nav-tabs menu3 text-center ">
									<li>
										<a href="#tab_default_2" data-toggle="tab">
										Articles </a>
									</li><br>
									<li class="">
										<a href="#tab_default_1" data-toggle="tab">
										Investor Education </a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tab_default_2">
										<h2 >
											Articles 
										</h2>

										<div class="card-columns">
											@forelse ($articles as $a)
											<div class="card img-loaded">
												<a href="{{ asset($a->ar_pdf) }}" target="_blank">
													<img class="card-img-top probootstrap-animate fadeIn probootstrap-animated" src="{{ asset($a->ar_image) }}" >
													<p class="text">Read Now</p>
												</a>
											</div>												
											@empty
											<div class="card img-loaded">
												No Article Found
											</div>
											@endforelse											
										</div>
									</div>
									<div class="tab-pane" id="tab_default_1">
										<h2>
											Investor Education
										</h2>
										<div class="card-columns">
											@forelse ($investorEducation as $ie)
											<div class="card img-loaded">
												<a href="#">
													<img class="card-img-top probootstrap-animate fadeIn probootstrap-animated" src="{{ asset($ie->ie_image) }}" alt="Card image cap" data-animate-effect="fadeIn">
												</a>
												<p class="text" onclick="openEnquiry('{{ $ie->ie_id }}', '{{ $ie->ie_title }}');">Read Now</p>
											</div>										
											@empty
											<div class="card img-loaded">
												No Investor Education Found
											</div>
											@endforelse
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</main>
@include('users.includes.enquiry')
@endsection