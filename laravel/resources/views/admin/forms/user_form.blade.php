@push('header')
@php
$ID = 'client';
@endphp
<script>
	ID = '{{ $ID }}';
</script>
@endpush

<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/formdata">	
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Client ID
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "ud[client_id]" value="{{ $user->detail->ud_client_id or '' }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Name
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "name" value="{{ $user->name or '' }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Mobile
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="number" min = "7000000000" max = "9999999999" class="form-control col-md-7 col-xs-12" name = "ud[mobile]" value="{{ $user->detail->ud_mobile or '' }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Alternate Mobile
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="number" min = "7000000000" max = "9999999999" class="form-control col-md-7 col-xs-12" name = "ud[alt_mobile]" value="{{ $user->detail->ud_alt_mobile or '' }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Email
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="email" class="form-control col-md-7 col-xs-12" name = "email" value="{{ $user->email or '' }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Portfolio Size
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="number" min = "0" max = "9999999999" class="form-control col-md-7 col-xs-12" name = "ud[portfolio_size]" value="{{ $user->detail->ud_portfolio_size or '' }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Address
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea class="form-control col-md-7 col-xs-12" name = "ud[address]">{{ $user->detail->ud_address or '' }}</textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-4 col-sm-4 col-xs-12">Date Inception
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="date" class="form-control col-md-7 col-xs-12" name = "ud[date_inception]" value="{{ $user->detail->ud_date_inception or '' }}">
		</div>
	</div>	
	@isset ($user)
	    <input type="hidden" name="sid" value = "{{ $user->id }}">
	@endisset
	<div class="ln_solid">
	</div>
	<div class="form-group text-center">							
		<button type="submit" class="btn btn-success">{{ isset($user) ? 'Update' : 'Add' }}</button>
	</div>					
</form>
@push('footer')
<script>		
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route('admin.user.update') }}',
		onLoad : '{{ isset($user) ? 'update' : 'add' }}',
		processResponse: function(data){
			if(data.msg == 'successU'){
				location.reload();
			}			
		}
	});
</script>
@endpush