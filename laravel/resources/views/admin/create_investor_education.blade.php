@extends('admin.layouts.master')
@section('title')
Create Investor Education
@endsection
@php
	$ID = 'investor-education';
@endphp
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3>Create Investor Education</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					@include('admin.forms.investor_education_form')
				</div>
			</div>
		</div>
	</div>
</div>

@endsection