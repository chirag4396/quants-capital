@extends('admin.layouts.master')
@section('title')
Import Weekly Report of {{ $user->name }}
@endsection

@php
$ID = 'import';
@endphp

@push('header')
<script>
	ID = '{{ $ID }}';
</script>
</style>
@endpush
@section('content')

<div class="right_col" role="main">	
	<div class="page-title">
		<div>
			<h3>
				Upload Excel
				<a href = "{{ route('admin.report',['id' => $user->id]) }}" class="btn btn-primary pull-right"><i class="fa fa-list"></i> Report</a>
			</h3>
		</div>		
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_content">					
					<div class="col-md-6">
						<div id = "uploadBox">
							<form action="{{ route('admin.upload-excel') }}" class="form-horizontal form-label-left" method="post"  enctype="multipart/form-data" id = "uploadForm">
								<div class="form-group">
									<label>Select Excel File:</label>
									<input type="file" name = "excel">
									<button type="submit" class="btn btn-primary">Upload</button>
								</div>										
							</form>
						</div>
						<div id = "importBox" class="hidden">
							<form action="{{ route('admin.import-data') }}" class="form-horizontal form-label-left" method="post">
								{{ csrf_field() }}

								<input type="hidden" name="excelFile" id = "excelFile">
								<input type="hidden" name="client" value="{{ $user->id }}">
								<button type="submit" class="btn btn-primary" id = "excelBtn">Import</button>
							</form>
						</div>
					</div>									
					<div class="col-md-6">
						<a href="{{ asset('excels/weekly-report-quants-capital-(sample).xlsx') }}" download class="btn btn-danger">Download Sample xls file</a>
					</div>							
				</div>
			</div>
			<div class="clearfix"></div>
			@if (Session::has('status'))
				<div class="alert alert-success">Successfully Imported, you can Upload any other Excel for {{ $user->name }}</div>
			@endif
		</div>
	</div>
</div>		
</div>
</div>

@endsection

@push('footer')
<script type="text/javascript">
	$('#uploadForm').CRUD({
		url : "{{ route('admin.upload-excel') }}",
		processResponse : function(data){    		
			if(data.msg == 'success'){
				$('#excelFile').val(data.file);
				$('#uploadBox').addClass('hidden');
				$('#importBox').removeClass('hidden');
			}
		}
	});	
	$('#excelBtn').on({
		'click' : function(){			
			$('#loader h1').html('Extracting Data from Excel, Please wait!!');
			$('#loader').removeClass('hidden');
		}
	});
</script>
@endpush