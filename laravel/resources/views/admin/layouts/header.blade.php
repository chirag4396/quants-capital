<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name') }} (Admin) | @yield('title')</title>
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/fav.png') }}">   

  <!-- Bootstrap -->
  <link href="{{ asset('admin-assets/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="{{ asset('admin-assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <!-- NProgress -->
  <link href="{{ asset('admin-assets/nprogress/nprogress.css') }}" rel="stylesheet">
  <!-- bootstrap-daterangepicker -->
  <link href="{{ asset('admin-assets/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

  <!-- Custom Theme Style -->
  <link href="{{ asset('admin-assets/css/custom.css') }}" rel="stylesheet">
  <style type="text/css">
    #loader{
      height: 100%;
      width: 100%;
      text-align: center;
      padding-top: 22%;
      position: fixed;
      background: #2A3F54;
      z-index: 9999;
    }
  </style>
@stack('header')
</head>

<body class="nav-md">
  <div id = "loader">
    <h1>Loading...</h1>
  </div>  
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="{{ route('admin.home') }}" class="site_title"> <span>{{ config('app.name') }}</span></a>
          </div>

          <div class="clearfix"></div>

          <!-- menu profile quick info -->
          <div class="profile clearfix">
            <div class="profile_pic">
              <img src="{{ asset('images/logo.png') }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>Welcome,</span>
              <h2>{{ Auth::user()->name }}</h2>
            </div>
          </div>
          <!-- /menu profile quick info -->

          <br />

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              {{-- <h3>General</h3> --}}
              <ul class="nav side-menu">                
                @if (Auth::user()->type == 101)
                <li><a href="{{ route('admin.clients') }}"><i class="fa fa-home"></i> Clients </a></li>
                <li><a><i class="fa fa-home"></i> Monthly Performance <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.monthly-performance.create') }}">Create Monthly Performance</a></li>
                    <li><a href="{{ route('admin.monthly-performance.index') }}">View Monthly Performance</a></li>
                  </ul>
                </li>                
                <li><a><i class="fa fa-home"></i>Articles <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.article.create') }}">Create Article</a></li>
                    <li><a href="{{ route('admin.article.index') }}">View Articles</a></li>
                  </ul>
                </li>             
                <li><a><i class="fa fa-home"></i>Investor Education <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('admin.investor-education.create') }}">Create Investor Education</a></li>
                    <li><a href="{{ route('admin.investor-education.index') }}">View Investor Educations</a></li>
                  </ul>
                </li>                
                <li><a href="{{ route('admin.enquiry.index') }}"><i class="fa fa-home"></i> Investor Education Enquiries </a></li>
                
                @else
                <li><a href="{{ route('admin.report', ['id' => Auth::id()]) }}"><i class="fa fa-home"></i> Weekly Report </a></li>
                @endif
              </ul>
            </div>
            

          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small hidden">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
              <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
              <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
              <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
              <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">
        <div class="nav_menu">
          <nav>
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
              <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Log Out <i class="fa fa-sign-out"></i> 
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form>                
              </li>                      
            </ul>
          </nav>
        </div>
      </div>
        <!-- /top navigation -->