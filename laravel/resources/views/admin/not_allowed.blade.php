@extends('admin.layouts.master')
@section('title')
Sorry {{ Auth::user()->name }}
@endsection
@php
$ID = 'client';
@endphp
@section('content')
<div class="right_col" role="main">		
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">                
					<h1>Sorry you are not allowed to view these report. <a href="{{ route('admin.report', ['id' => Auth::id()]) }}"><i class="fa fa-home"></i> click here to check yours </a></h1>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection