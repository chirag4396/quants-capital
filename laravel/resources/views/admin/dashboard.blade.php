@extends('admin.layouts.master')

@section('content')
<div class="right_col" role="main">
  <div class="">
    <div class="row top_tiles">
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('admin.clients') }}">
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-list"></i></div>
            <div class="count">{{ App\User::where('type',102)->count() }}</div>
            <h3>All Clients</h3>          
          </div>
        </a>
      </div>
      <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <a href="{{ route('admin.clients',['type'=> 0, 'name' => 'not-approved']) }}">        
          <div class="tile-stats">
            <div class="icon"><i class="fa fa-list"></i></div>
            <div class="count">{{ App\User::where(['type' => 102, 'approved' => 0])->count() }}</div>
            <h3>New Request of Clients</h3>
          </div>
        </a>
      </div>            
    </div>
  </div>
</div>
@endsection