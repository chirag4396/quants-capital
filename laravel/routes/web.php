<?php

Route::get('/', 'HomeController@index')->name('home');

Route::get('/why', function () {
	return view('users.why');
})->name('why');

Route::get('/chart', function () {
	return view('users.chart');
})->name('chart');

Route::get('/about', 'HomeController@about')->name('about');
Route::get('/insights', 'HomeController@insight')->name('insight');
Route::get('/contact', 'HomeController@contact')->name('contact');
// Route::post('/enquiry', 'HomeController@enquiry')->name('enquiry.store');

Route::get('/what-we-offer', 'HomeController@what_we_offer')->name('what-we-offer');

Auth::routes();

Route::get('/admin/login', function() {
	return view('admin.login');
});
Route::resource('enquiry', 'EnquiryController');

Route::get('monthly-performance', 'MonthlyPerformanceController@getAll')->name('monthly-performance');

Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'as' => 'admin.'], function(){	
	Route::get('/', 'UserController@admin')->name('home');
	Route::resource('enquiry', 'EnquiryController');
	Route::resource('monthly-performance', 'MonthlyPerformanceController');
	Route::resource('article', 'ArticleController');
	Route::resource('investor-education', 'InvestorEducationController');
	Route::get('clients/{type?}/{name?}', 'UserController@clients')->name('clients');
	Route::delete('client/{id}', 'UserController@destroy')->name('user.destroy');
	Route::get('client/{id}/edit', 'UserController@edit')->name('user.edit');
	Route::post('client', 'UserController@update')->name('user.update');
	Route::get('user-update/{id}/{status}', 'UserController@changeStatus')->name('change-status');
	Route::get('report/{id}', 'UserController@report')->name('report');
	Route::get('import/{id}', 'ImportController@import')->name('import');
	Route::post('upload-excel/', 'ImportController@uploadFile')->name('upload-excel');
	Route::post('import/', 'ImportController@importData')->name('import-data');	
	
});