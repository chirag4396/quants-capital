<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
	public $primaryKey = 'rep_id';

	protected $fillable = ['rep_date', 'rep_added_by', 'rep_client', 'rep_booked_p_l', 'rep_month', 'rep_year'];

	CONST CREATED_AT = 'rep_created_at';

	CONST UPDATED_AT = null;

	public function user()
	{
		return $this->belongsTo(\App\User::class, 'rep_client');
	}

	public function nifty()
	{
		return $this->hasOne(\App\NiftyReport::class, 'nr_rep');
	}

	public function openPositions()
	{
		return $this->hasMany(\App\OpenPosition::class, 'op_rep');
	}

	public function strategy()
	{
		return $this->hasOne(\App\StrategyReport::class, 'st_rep');
	}

	public function current_p_n_l()
	{
		return $this->strategy();
	}
}
