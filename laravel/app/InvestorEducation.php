<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestorEducation extends Model
{
	public $primaryKey = 'ie_id';

	protected $fillable = ['ie_title','ie_image', 'ie_delete'];

	CONST CREATED_AT = 'ie_created_at';

	CONST UPDATED_AT = null;
}
