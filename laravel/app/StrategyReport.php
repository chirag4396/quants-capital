<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StrategyReport extends Model
{
    public $primaryKey = 'st_id';

    protected $fillable = ['st_margin_deployed', 'st_current_p_n_l', 'st_per_on_open_position', 'st_net_gain', 'st_per_overall_portfolio', 'st_rep'];

    CONST CREATED_AT = 'st_created_at';

    CONST UPDATED_AT = null;

    public function report()
    {
    	return $this->belongsTo(\App\Report::class, 'st_rep');
    }
}
