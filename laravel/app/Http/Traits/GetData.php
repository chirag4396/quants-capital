<?php 
namespace App\Http\Traits;
use App\CandidateDetail;
use App\Designation;
use App\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\ImageManager;

trait GetData{

	public function checkuser($req){
		
		return $candidate = CandidateDetail::where('can_mobile',$req)->first();		
				
	}

	public function getDesgination($d){		
		$des = Designation::where('des_name' , $d)->first();
		if($des){
			return $des->des_id;
		}else{
			return Designation::create(['des_name' => $d])->des_id;
		}
	}

	public function resizeImage($image, $quality = 75){
	// return $image;   
		$sourceImage = $image;
		$targetImage = $image;

		list($maxWidth, $maxHeight, $type, $attr) = getimagesize($image);
		
		if (!$image = @imagecreatefromjpeg($sourceImage)){
			return false;
		}

		list($origWidth, $origHeight) = getimagesize($sourceImage);

		if ($maxWidth == 0){
			$maxWidth  = $origWidth;
		}

		if ($maxHeight == 0){
			$maxHeight = $origHeight;
		}

		$widthRatio = $maxWidth / $origWidth;
		$heightRatio = $maxHeight / $origHeight;

		$ratio = min($widthRatio, $heightRatio);

		$dataewWidth  = (int)$origWidth  * $ratio;
		$dataewHeight = (int)$origHeight * $ratio;

		$dataewImage = imagecreatetruecolor($dataewWidth, $dataewHeight);
		imagecopyresampled($dataewImage, $image, 0, 0, 0, 0, $dataewWidth, $dataewHeight, $origWidth, $origHeight);
		imageinterlace($image, 1);
		imagejpeg($dataewImage, $targetImage, $quality);

		imagedestroy($image);
		imagedestroy($dataewImage);
	}

	public function changeKeys($rep, $arr = []){
		$dataew = [];
		foreach ($arr as $key => $value) {			
			$dataew[$rep.$key] = $value;
		}		
		return $dataew;
	}

	public function dateParser($date){
		return Carbon::parse($date)->format('Y-m-d h:i:s');
	}

	public function removePrefix($data){
		foreach ($data as $k => $v) {
			if(is_array($v)){
				$data[$k] = $this->removePrefix($v);
			}

			$e = explode('_', $k);
						
			if(count($e) > 1){
				unset($e[0]);				
				$d = implode('_', $e);
				$data[$d] = $v;
				unset($data[$k]);
			}
		}
		return $data;        
	}
	
	public function randomPassword($length) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$password = substr( str_shuffle( $chars ), 0, $length );
		return $password;
	}   

	public function username(){     
		return 'myfab'.(User::max('id')+1);
	}

	public function addUser($req){		
		$user = User::orWhere('email', $req['email'])                
		->first();

		$pass = $this->randomPassword(8);

		if(!$user){
			$newUser = User::create([				
				'name' => $req['firstname'],				
				'mobile' => $req['phone'],
				'email' => $req['email'],     
				'password' => bcrypt($pass),
				'visible_password' => $pass,
				'type' => 102,				
			]);
            $this->sendEmail($req['email'], $req['firstname'], ['email' => $newUser->email, 'pass' => $newUser->visible_password], 'new_user', ' Account Details');

			$this->guard()->login($newUser);
			return $newUser->id;
		}else{
			$this->guard()->login($user);			
			return $user->id;   
		}
	}

	public function removeFile($path){
		if(File::exists($path)){
			unlink($path);
		}
	}

	public function uploadFiles($data, $title, $file, $paths = [], $remove = []){
		// return $paths;
		$postfix = ['big','thumbnail'];
		$da = [];

		$title = strtolower(str_replace(' ','-',trim($title))).'-'.rand(9999,4);
		$exe = $data->file($file)->getClientOriginalExtension();
		$manager = new ImageManager();
		
		$photo = $data->file($file)->getRealPath();
		
		foreach ($paths as $key => $value) {
			if(count($remove) > 0){
				$this->removeFile($remove[$key]);
			}
			$filename = $title.'-'.$postfix[$key].'.'.$exe;
			$full = $paths[$key].$filename;            

			$manager->make($photo)->save($full);
			// ->resize($width, null, function ($constraint) {
			// 	$constraint->aspectRatio();
			// })

			$this->resizeImage($full);
			$da[] = $full;
		}
		return $da;
	}

	public function sendSMS($mobile,$msg){

		$url = 'http://trans.smsfresh.co/api/sendmsg.php';

		$fields = array(
			'user' => 'magarsham', 
			'pass' => 'festivito5555', 
			'sender' => 'FESTVT' ,
			'phone' => $mobile, 
			'text' => $msg, 
			'priority' => 'ndnd', 
			'stype' => 'normal'        
		);


		$ch = curl_init();


		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));


		$res = curl_exec($ch);


		curl_setopt($ch, CURLOPT_TIMEOUT, 0);
		curl_close($ch);

		return $res;
	}

	protected function guard()
	{
		return Auth::guard();
	}


	public function sendEmail($email, $name, $data, $blade, $sub = 'Order confirmation'){
		// return $data;
		// return view('admin.email.notification')->with(['id' => $orderId, 'msg' => $msg]);
		
		// $order = OrderDetail::find($orderId);
		return Mail::send('admin.email.'.$blade, $data, function ($message) use ($email, $name, $sub) {
			$message->from('info@successacademy.com', 'Success Academy');
			$message->sender('chirag.sungare@gmail.com', 'Success Academy');
			$message->to($email, $name);			    
			
			$message->subject('Success Academy - '.$sub);
			
			$message->priority(1);				   
		});
			// return $link;
	}
}