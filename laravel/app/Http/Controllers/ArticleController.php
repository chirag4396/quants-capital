<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class ArticleController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    protected $path = 'images/articles/';
    
    protected $pdf_path = 'images/articles/pdfs/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_articles')->with(['articles' => Article::where('ar_delete', 0)->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_article');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        try {
            $ar = $this->changeKeys('ar_', $r->all());
            $ar['ar_title'] = ucfirst($ar['ar_title']);                        
            if ($r->hasFile('image')) {
                list($regular) = $this->uploadFiles($r, $ar['ar_title'], 'image', [$this->path]);
                $ar['ar_image'] = $regular;                    
            }
            if ($r->hasFile('pdf')) {
                $exe = $r->file('pdf')->getClientOriginalExtension();
                $filename = strtolower(str_replace(' ', '-', $ar['ar_title'])).'.'.$exe;
                $r->file('pdf')->move($this->pdf_path, $filename);
                $ar['ar_pdf'] = $this->pdf_path.$filename;
            }
            $this->res['msg'] = Article::create($ar) ? 'success' : 'error';
            
        } catch (QueryException $e) {
            $this->res['error'] = $e->getMessage();
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        return view('admin.edit_article')->with(['article' => $article]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, Article $article)
    {
        try {
            $ar = $this->changeKeys('ar_', $r->all());
            $ar['ar_title'] = ucfirst($ar['ar_title']);
            if ($r->hasFile('image')) {
                list($regular) = $this->uploadFiles($r, $ar['ar_title'], 'image', [$this->path]);
                $ar['ar_image'] = $regular;
                $this->removeFile($article->ar_image);
            }
            if ($r->hasFile('pdf')) {
                $exe = $r->file('pdf')->getClientOriginalExtension();
                $filename = strtolower(str_replace(' ', '-', $ar['ar_title'])).'.'.$exe;
                $r->file('pdf')->move($this->pdf_path, $filename);
                $ar['ar_pdf'] = $this->pdf_path.$filename;
                $this->removeFile($article->ar_pdf);
            }
            $this->res['msg'] = $article->update($ar) ? 'successU' : 'error';
            
        } catch (QueryException $e) {
            $this->res['error'] = $e->getMessage();
        }
        return $this->res;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->update(['ar_delete' => 1]);
        return redirect()->back()->with(['article' => Article::where('ar_delete', 0)->get()]);
    }
}
