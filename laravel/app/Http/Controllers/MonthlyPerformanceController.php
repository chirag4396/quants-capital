<?php

namespace App\Http\Controllers;

use App\MonthlyPerformance;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class MonthlyPerformanceController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function transposeData($data)
{
    $retData = array();
    foreach ($data as $row => $columns) {
        foreach ($columns as $row2 => $column2) {
            $retData[$row2][$row] = $column2;
        }
    }
    return $retData;
}
public function getAll(){
    $monthlyPerformance = MonthlyPerformance::get();
    return $monthlyPerformance->toArray();
}
public function index()
{
    $monthlyPerformance = MonthlyPerformance::get();
    return view('admin.view_performances')->with(['monthlyPerformance' => $monthlyPerformance]);
}

/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
    return view('admin.create_monthly_performance');
}

/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $r)
{
    try {
        $mp = $this->changeKeys('mp_', $r->all());
        $mp['mp_added_by'] = Auth::id();
        if(MonthlyPerformance::create($mp)){
            $this->res['msg'] = 'success';
        }
    } catch (QueryException $e) {
        $this->res['msg'] = 'error';        
        $this->res['error'] = $e->getMessage(); 
    }
    return $this->res;
}

/**
* Display the specified resource.
*
* @param  \App\MonthlyPerformance  $monthlyPerformance
* @return \Illuminate\Http\Response
*/
public function show(MonthlyPerformance $monthlyPerformance)
{
//
}

/**
* Show the form for editing the specified resource.
*
* @param  \App\MonthlyPerformance  $monthlyPerformance
* @return \Illuminate\Http\Response
*/
public function edit(MonthlyPerformance $monthlyPerformance)
{
    return view('admin.edit_monthly_performance')->with(['mp' => $monthlyPerformance]);
}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\MonthlyPerformance  $monthlyPerformance
* @return \Illuminate\Http\Response
*/
public function update(Request $r, MonthlyPerformance $monthlyPerformance)
{
    try {
        $mp = $this->changeKeys('mp_', $r->all());            
        if($monthlyPerformance->update($mp)){
            $this->res['msg'] = 'successU';
        }
    } catch (QueryException $e) {
        $this->res['msg'] = 'error';        
        $this->res['error'] = $e->getMessage(); 
    }
    return $this->res;
}

/**
* Remove the specified resource from storage.
*
* @param  \App\MonthlyPerformance  $monthlyPerformance
* @return \Illuminate\Http\Response
*/
public function destroy(MonthlyPerformance $monthlyPerformance)
{
//
}
}
