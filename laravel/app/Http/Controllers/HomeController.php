<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MonthlyPerformance;
use App\Article;
use App\InvestorEducation;
class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('users.index');
    }

    public function what_we_offer(){
        return view('users.what-we-offer')->with(['monthlyPerformances' => MonthlyPerformance::get()]);
    }

    public function about() {
        return view('users.about');
    }

    public function insight() {
        return view('users.insight')->with(['investorEducation' => InvestorEducation::where(['ie_delete' => 0])->get(), 'articles' => Article::where(['ar_delete' => 0])->get()]);
    }    
    
    public function contact() {
        return view('users.contact');
    }    

    public function enquiry(Request $r) {
        return $r->all();
    }    
}
