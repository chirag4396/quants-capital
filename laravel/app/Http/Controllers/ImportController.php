<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Report;
use App\NiftyReport;
use App\StrategyReport;
use App\OpenPosition;
use App\User;
class ImportController extends Controller
{
    protected $client;
    public function import($id)
    {
        if(Auth::user()->type == 101){
            return view('admin.import')->with(['user' => User::find($id)]);
        }
        return redirect()->back()->with(['clients' => User::where(['type' => 102, 'soft_delete' => 0])->get()]);
    }
    public function uploadFile(Request $r){
        $title = uniqid();
        $exe = $r->file('excel')->getClientOriginalExtension();
        $m = 'excels';
        $filename = $title.'.'.$exe;
        $r->file('excel')->move($m,$filename);
        return ['msg' => 'success', 'file' => $filename];
    }

    public function sortValues($reader){
        $finalData =[];
        $sheet_name = strtolower(trim($reader->getTitle()));
        $sheet_date = Carbon::parse($sheet_name);
        $oldReport = Report::where('rep_date',$sheet_date)->first();
        
        if($oldReport){
            $oldReport->nifty()->delete();
            $oldReport->strategy()->delete();
            $oldReport->openPositions()->delete();
            $oldReport->delete();
        }

        $finalData['sheet'] = [
            'rep_date' => $sheet_date->toDateString(),
            'rep_client' => $this->client,                    
            'rep_added_by' => Auth::id(),
            'rep_month' => $sheet_date->format('m'),
            'rep_year' => $sheet_date->format('Y')
        ];
        $nKeys = [
            10 => 'nr_closing_previous',
            11 => 'nr_current_week',
            12 => 'nr_percent_return_last_week',
            13 => 'nr_time_of_inception',
            14 => 'nr_percent_return'
        ];
        $sKeys = [
            10 => 'st_margin_deployed',
            11 => 'st_current_p_n_l',
            12 => 'st_per_on_open_position',
            13 => 'st_net_gain',
            14 => 'st_per_overall_portfolio'
        ];        
        $row = 1;
        foreach ($reader as $key => $value) {
            if ($value->a) {
                if ($row == 6) {
                    $finalData['sheet']['rep_booked_p_l'] = $value->b;
                }
                if($row > 9 && $row < 15){
                    // echo $value->b;
                    if(in_array($row, [14,12])){
                        $finalData['nifty'][$nKeys[$row]] = is_numeric($value->b) ? round($value->b * 100, 2):0;
                        $finalData['strategy'][$sKeys[$row]] = is_numeric($value->d) ? round($value->d * 100, 2):0;
                    }else{
                        $finalData['nifty'][$nKeys[$row]] = is_numeric($value->b) ? $value->b:0;
                        $finalData['strategy'][$sKeys[$row]] = is_numeric($value->d) ? $value->d:0;
                    }
                }

                if($row > 18){
                    $finalData['op'][] = [
                        'op_days' => $value->a,
                        'op_ser_exp' => Carbon::parse($value->b)->toDateString(),
                        'op_strike_price' => $value->c,
                        'op_option_type' => $value->d,
                        'op_units' => $value->e,
                        'op_trade_price' => $value->f,
                        'op_theoritical_price' => $value->g,
                        'op_realized_iv' => $value->h,
                        'op_last_iv' => $value->i,
                        'op_ltp' => $value->j
                    ];
                }
            }
            $row++;
        }
        $r = Report::create($finalData['sheet']);
        if($r){                 
            $finalData['nifty']['nr_rep'] = $r->rep_id;
            $finalData['strategy']['st_rep'] = $r->rep_id;                        

            NiftyReport::create($finalData['nifty']); 
            StrategyReport::create($finalData['strategy']);
            foreach ($finalData['op'] as $opKey => $opValue) {
                $opValue['op_rep'] = $r->rep_id;
                OpenPosition::create($opValue);
            }                        
        }
        return;
    }
    public function importData(Request $r)
    {    		
        $path = 'excels/'.$r->excelFile;        
        $data = Excel::load($path, function($reader) {})->get();

        $count = 0;
        $this->client = $r->client;
        Excel::load($path, function($reader) {  
            $sheet_count = $reader->getSheetCount();
            if($sheet_count<=1){
                print_r($this->sortValues($reader->get()));
            }else{
                $reader->each(function($reader) {
                    $this->sortValues($reader);
                });
            }                    
        });

        unlink($path);

        return redirect()->back()->with(['status' => 'done']);                    
    }
}
