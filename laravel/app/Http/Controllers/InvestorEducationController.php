<?php

namespace App\Http\Controllers;

use App\InvestorEducation;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class InvestorEducationController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    protected $path = 'images/investor-educations/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_investor_education')->with(['investorEducations' => InvestorEducation::where('ie_delete', 0)->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_investor_education');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        try {
            $ie = $this->changeKeys('ie_', $r->all());
            $ie['ie_title'] = ucfirst($ie['ie_title']);                        
            if ($r->hasFile('image')) {
                list($regular) = $this->uploadFiles($r, $ie['ie_title'], 'image', [$this->path]);
                $ie['ie_image'] = $regular;                    
            }            
            $this->res['msg'] = InvestorEducation::create($ie) ? 'success' : 'error';
            
        } catch (QueryException $e) {
            $this->res['error'] = $e->getMessage();
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvestorEducation  $investorEducation
     * @return \Illuminate\Http\Response
     */
    public function show(InvestorEducation $investorEducation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvestorEducation  $investorEducation
     * @return \Illuminate\Http\Response
     */
    public function edit(InvestorEducation $investorEducation)
    {
        return view('admin.edit_investor_education')->with(['investorEducation' => $investorEducation]);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvestorEducation  $investorEducation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, InvestorEducation $investorEducation)
    {
        try {
            $ie = $this->changeKeys('ie_', $r->all());
            $ie['ie_title'] = ucfirst($ie['ie_title']);
            if ($r->hasFile('image')) {
                list($regular) = $this->uploadFiles($r, $ie['ie_title'], 'image', [$this->path]);
                $ie['ie_image'] = $regular;
                $this->removeFile($investorEducation->ie_image);
            }            
            $this->res['msg'] = $investorEducation->update($ie) ? 'successU' : 'error';
            
        } catch (QueryException $e) {
            $this->res['error'] = $e->getMessage();
        }
        return $this->res;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvestorEducation  $investorEducation
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvestorEducation $investorEducation)
    {
        $investorEducation->update(['ie_delete' => 1]);
        return redirect()->back()->with(['investorEducation' => InvestorEducation::where('ie_delete', 0)->get()]);
    }
}
