<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Traits\GetData;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Database\QueryException;
class UserController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    public function clients($type = null, $name = null) {
        $clients = User::where(['type' => 102, 'soft_delete' => 0]);
        if(!is_null($type)){
            $clients = $clients->where(['approved' => $type]);
        }
        $clients = $clients->get();
        return view('admin.view_clients')->with(['clients' => $clients,'type' => $type]);
    }    

    public function report($id) {
        if(Auth::id() == $id || Auth::user()->type == 101){
            $years = DB::table('users')->join('reports', 'reports.rep_client', '=', 'users.id')->select(DB::raw('YEAR(reports.rep_date) AS year'))->groupBy(['year'])->orderBy('year', 'desc')->get();
            return view('admin.view_report')->with(['user' => User::find($id), 'years' => $years]);
        }else{
            return view('admin.not_allowed');
        }
    }
    public function admin() {
        if(Auth::user()->type == 101){
            return view('admin.dashboard');
        }else{
            return redirect()->route('admin.report', ['id'=>Auth::id()]);
        }
    }
    public function changeStatus($id, $status){
        User::find($id)->update(['approved' => $status]);
        return redirect()->back()->with(['clients' => User::where('type', 102)->get()]);
    }

    public function edit($id)
    {
        return view('admin.edit_user')->with(['user' => User::find($id)]);
    }

    public function update(Request $r) {
        try {
            if(!User::where('email', $r->email)->where('id','!=',$r->sid)->first()) {
                $user = User::find($r->sid);
                $ud = $this->changeKeys('ud_',$r->all()['ud']);        
                unset($r->all()['ud']);
                $user->update($r->all());
                $user->detail->update($ud);
                $this->res['msg'] = 'successU';
            }else{
                $this->res['msg'] = 'existEmail';
            }

        } catch (QueryException $e) {
            $this->res['error'] = $e->getMessage();
        }
        return $this->res;
    }

    public function destroy($id)
    {
        // return $user;
        User::find($id)->update(['soft_delete' => 1]);
        $clients = User::where(['type' => 102, 'soft_delete' => 0])->get();
        // return view('admin.view_clients')->with(['clients' => $clients]);
        return redirect()->back()->with(['clients' => $clients]);
    }
}
