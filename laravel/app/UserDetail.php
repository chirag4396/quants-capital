<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    public $primaryKey = 'ud_id';

    protected $fillable = ['ud_mobile', 'ud_alt_mobile', 'ud_address', 'ud_user_id', 'ud_portfolio_size', 'ud_client_id', 'ud_date_inception', 'ud_security_question', 'ud_security_answer'];
    
    public $timestamps = false;

    public function user(){
    	return $this->belongsTo(\App\User::class, 'ud_user_id');
    }
}
