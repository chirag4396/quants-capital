<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'approved', 'soft_delete'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function detail() {
        return $this->hasOne(\App\UserDetail::class, 'ud_user_id');
    }

    public function reports($month, $year) {
        return $this->hasMany(\App\Report::class, 'rep_client')->where(['rep_month' => $month, 'rep_year' => $year])->orderBy('rep_created_at', 'desc');
    }
}
