<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public $primaryKey = 'ar_id';

    protected $fillable = ['ar_title', 'ar_pdf', 'ar_image', 'ar_delete'];

    CONST CREATED_AT = 'ar_created_at';

    CONST UPDATED_AT = null;
}
