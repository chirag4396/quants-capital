<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenPosition extends Model
{
	public $primaryKey = 'op_id';

	protected $fillable = ['op_days', 'op_ser_exp', 'op_strike_price', 'op_option_type', 'op_units', 'op_trade_price', 'op_theoritical_price', 'op_realized_iv', 'op_last_iv', 'op_ltp', 'op_rep'];

	CONST CREATED_AT = 'op_created_at';

	CONST UPDATED_AT = null;

	public function report()
	{
		return $this->belongsTo(\App\Report::class, 'op_rep');
	}
}
