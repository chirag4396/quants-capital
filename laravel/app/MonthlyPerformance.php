<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyPerformance extends Model
{
    public $primaryKey = 'mp_id';

    protected $fillable = ['mp_date', 'mp_dns', 'mp_nifty', 'mp_nifty_dns', 'mp_fd', 'mp_fd_dns', 'mp_created_at', 'mp_added_by'];

    CONST CREATED_AT = 'mp_created_at';

    CONST UPDATED_AT = 'mp_updated_at';

    public function added(){
    	return $this->belongsTo(\App\User::class, 'mp_added_by');
    }
}
