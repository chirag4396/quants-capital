<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NiftyReport extends Model
{
    public $primaryKey = 'nr_id';

    protected $fillable = ['nr_id', 'nr_closing_previous', 'nr_current_week', 'nr_percent_return_last_week', 'nr_time_of_inception', 'nr_percent_return', 'nr_rep'];

    CONST CREATED_AT = 'nr_created_at';

    CONST UPDATED_AT = null;

    public function report()
    {
    	return $this->belongsTo(\App\Report::class, 'nr_rep');
    }
}
