<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
	public $primieyKey = 'enq_id';

	protected $fillable = ['enq_name','enq_mobile', 'enq_email', 'enq_ie_id'];

	CONST CREATED_AT = 'enq_created_at';

	CONST UPDATED_AT = null;

	public function book(){
		return $this->belongsTo(\App\InvestorEducation::class, 'enq_ie_id');
	}
}
