<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecurityQuestion extends Model
{
    public $primaryKey = 'sq_id';

    protected $fillable = ['sq_qus'];

    public $timestamps = false;
}
